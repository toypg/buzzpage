//
//  ViewController.h
//  ProjApp02
//
//  Created by 岡室 庄悟 on 2013/07/08.
//  Copyright (c) 2013年 岡室 庄悟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import <CoreLocation/CoreLocation.h>

@class SBJsonStreamParser;
@class SBJsonStreamParserAdapter;



@interface ViewController : UIViewController<CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    
    IBOutlet UILabel *allstar01;
    IBOutlet UILabel *label01;
    IBOutlet UILabel *hoshi01;
    IBOutlet UILabel *hoshi02;
    IBOutlet UILabel *label02;
    IBOutlet UILabel *hoshi03;
       
    IBOutlet UILabel *label03;
    
    
    
    // ここからjsonデータを利用するための変数です。
    NSString* fileName;
    NSString* path;
    NSData* data;
    NSArray* jsonAry;
    float jsonLat;
    float jsonLon;
    NSString* strLat;
    NSString* strLon;
    NSString* dataStr;
    NSURLRequest* request;
    SBJsonParser* sbjsonparser;
    NSError* error;
    NSDictionary* dic;
    
    NSString* strstar;
    int jsonstar;
    NSString* strcome;
    NSString* come[3];
    int intstar[3];
    NSString* star[3];
    NSString* kuro;
    NSString* shiro;
    int avr,sum;
    NSString* allstar;
}


- (void)shutoku;//メソッドの定義
- (void)byouga;//描画メソッドも定義

@end
