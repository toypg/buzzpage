//
//  BlackStroke.m
//  ProjApp02
//
//  Created by 岡室 庄悟 on 2013/07/14.
//  Copyright (c) 2013年 岡室 庄悟. All rights reserved.
//

#import "BlackStroke.h"

@implementation BlackStroke

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = UIColor.clearColor; //背景を透明に
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();  // コンテキストを取得
    
    // 線を黒色にする
    CGContextSetStrokeColorWithColor(context,UIColor.grayColor.CGColor);
    
    
    // 線の太さを指定
    CGContextSetLineWidth(context, 5.0);  // 10ptに設定
    
    CGContextMoveToPoint(context, 10, 300);  // 始点
    CGContextAddLineToPoint(context, 300, 300); // 終点
    CGContextStrokePath(context);  // 描画
    // Drawing code
}


@end
