//
//  KakuninViewController.m
//  ProjApp02
//
//  Created by 岡室 庄悟 on 2013/07/16.
//  Copyright (c) 2013年 岡室 庄悟. All rights reserved.
//

#import "KakuninViewController.h"

@interface KakuninViewController ()

@end

@implementation KakuninViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_ktitle release];
    [_kcome release];
    [_kstar release];
    [super dealloc];
}
@end
