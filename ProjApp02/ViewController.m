//
//  ViewController.m
//  ProjApp02
//
//  Created by 岡室 庄悟 on 2013/07/03.
//  Copyright (c) 2013年 岡室 庄悟. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//int hoshi[];//星の数を入れる配列です
//NSString *hyouka,*komento[];//上の評価と各コメントが入る配列。
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"ViewdidLoad");
    
  //  

    //request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apis.jarupy.net/api/v1/index.json"]];
    
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apis.jarupy.net/api/v1/buzz.json"]];
    // NSDataでjsonデータを取得
    data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    // ここまでweb上から読み込む方法 //
    
    
    // 読み込めなかったら強制終了
    if(data == nil) abort();
    
    
    
    // データを文字列にエンコードしています。
    dataStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"data = %@", dataStr);
    
    // SBJsonでパース（解析）する
    // NSDictionaryの記述で、ネット上のサンプル群はerrorを記述しているが、不要になった？errorを書くとエラーを吐かれる
    sbjsonparser = [[SBJsonParser alloc] init];
    error = nil;
    dic = [sbjsonparser objectWithString:dataStr];
    
    //NSLog(@"JSON dictionary = %@",[dic description]);
    //NSLog(@"%@",dic);
    //strLat = (NSString *)[nextKey objectForKey:@"lat"];
    //NSLog(@"dic=%@",[dic objectForKey:@"number"] );
    //NSLog(@"%@",[dic emoveObjectForKey:@"number"]);
    int i=0;
    for (id key in [dic objectForKey:@"buzzs"]) {
        //NSLog(@"for1");
        //for//
            //NSLog(@"for2");
            //strLat = (NSString *)[nextKey objectForKey:@"lat"];
            //strLon = (NSString *)[nextKey objectForKey:@"lon"];
            //jsonLat = strLat.floatValue;
            //jsonLon = strLon.floatValue;
            //marker.title = (NSString *)[nextKey objectForKey:@"name"];
            
        strstar = (NSString *)[key objectForKey:@"star"];//jsonファイルから取得
        strcome = (NSString *)[key objectForKey:@"contents"];//jsonファイルから取得
        jsonstar = strstar.intValue;//int型に変換
           // strstar = (NSString *)[nextKey objectForKey:@"star"];
        NSLog(@"jsonstar01=%d",jsonstar);
        NSLog(@"strcome%@",strcome);
        intstar[i]=jsonstar;
        come[i]=strcome;
        sum+=jsonstar;
        i++;
        //NSLog(@"come%@",come[0]);
        /*jsonの数によってlabelを自動生成したいなぁ・・・・・
        UILabel *titleLabel = [[[UILabel alloc] init] autorelease];
        titleLabel.frame = CGRectMake(5, 10, (self.view.bounds.size.width-20), 25);
        titleLabel.text = @"タイトルらべる";
        */
        
        
       // }
    }
   // NSLog(@"jsonstar=%d",jsonstar);
    
    //[self shutoku];
    [self byouga];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)byouga {//描画します
    NSLog(@"byouga");
    int count;
    label01.text=come[0];//とりあえずコメントを描画
    label02.text=come[1];
    label03.text=come[2];
    //star=@"";
    kuro=@"★";
    shiro=@"☆";
    //NSLog(@"%d",jsonstar);
    for(int j=0;j<3;j++){
        star[j]=@"";
        for(count=0;count<intstar[j];count++){//ここのfor文２つで星をつくります。
            
            star[j] = [NSString stringWithFormat:@"%@ %@",star[j],kuro];
        }
        for(int i=0;i<=4-count;i++){
            star[j] = [NSString stringWithFormat:@"%@ %@",star[j],shiro];
        }
    }
    
    hoshi01.text=star[0];//星を描画します
    hoshi02.text=star[1];
    hoshi03.text=star[2];
    avr=sum/3;
    NSLog(@"avr=:%d",avr);
    allstar=@"";
    for(count=0;count<avr;count++){//ここのfor文２つで星をつくります。
        
        allstar= [NSString stringWithFormat:@"%@ %@",allstar,kuro];
        }
        for(int i=0;i<=4-count;i++){
            allstar= [NSString stringWithFormat:@"%@ %@",allstar,shiro];
        }
    
    allstar01.text=allstar;
}


/*
- (void)shutoku{//全体の評価に加えて、個人の星と評価を取得する
    NSString *review[]={@"aaaaaa",@"iiii",@"uuuu",@"eeeeee"};
    int sum=0,i,allstar;//jsonからとれたとして。
    hyouka=@"ここのトイレは気持ちいです";//全体の評価
    srand((unsigned)time(NULL));//ここからは個人の星とコメント
    
    for(i=0;i<4;i++){
        int n = random()%5+1;
        hoshi[i]=n;//星の数
        sum+=n;
        NSLog(@"%d",n);
        komento[i]=review[i];
    }
    allstar=sum/(i+1);
    //label01.text=review[1];
    
}
*/

- (void)dealloc {
    [label01 release];
    [hoshi01 release];
    [hoshi02 release];
    [label02 release];
    [hoshi03 release];
    [label03 release];
    [allstar01 release];
    [super dealloc];
}
@end
