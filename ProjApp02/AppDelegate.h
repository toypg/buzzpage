//
//  AppDelegate.h
//  ProjApp02
//
//  Created by 岡室 庄悟 on 2013/07/08.
//  Copyright (c) 2013年 岡室 庄悟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
